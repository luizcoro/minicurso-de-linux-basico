#include <iostream>
#include <limits>
#include <unistd.h>

std::string encomoda(int n)
{

    if(n == 1)
        return "encomoda";

    std::string encomodam = "encomodam";

    for(int i = 1; i < n; ++i)
        encomodam += ", encomodam";

    return encomodam;
}

int main(int argc, char *argv[])
{
    unsigned int two_seconds = 2000000;
    unsigned int four_seconds = 4000000;

    std::cout << "1 elefante encomoda muita gente" << std::endl;
    usleep(two_seconds);

    for(int i = 2; i <  std::numeric_limits<int>::max(); ++i)
    {
        if(i % 2 != 0)
        {
            std::cout << i << " elefantes encomodam muita gente" << std::endl;
            usleep(two_seconds);
        }
        else
        {
            std::cout << i << " elefantes " << encomoda(i) << " muito mais" << std::endl << std::endl;
            usleep(four_seconds);
        }

    }

    return 0;
}
