.PHONY: clean cleanall

BUILDDIR := $(realpath ./build)
OUTDIR := .

all:
	@echo "$(BUILDDIR)"
	@mkdir -p $(BUILDDIR)
	@echo " Compiling aula1..."
	cd src/aula1 && pdflatex --output-directory=$(BUILDDIR) aula1.tex && pdflatex --output-directory=$(BUILDDIR) aula1.tex && cd ../..
	@echo " Compiling aula2..."
	cd src/aula2 && pdflatex --output-directory=$(BUILDDIR) aula2.tex && pdflatex --output-directory=$(BUILDDIR) aula2.tex && cd ../..
	@echo " Copying to output directory"
	cp $(BUILDDIR)/aula1.pdf $(BUILDDIR)/aula2.pdf $(OUTDIR)

clean:
	@echo " Cleaning..."
	$(RM) -r $(BUILDDIR)

cleanall:
	@echo " Cleaning..."
	$(RM) -r $(BUILDDIR) aula1.pdf aula2.pdf
